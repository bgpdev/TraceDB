package system.database;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Represents the Database connection to store persistent information.
 */
public class Database
{
    /** Holds the connection with the database. */
    private Connection connection;

    /**
     * Construct a Database connection.
     * @param url The URL of the database connection.
     * @param database The name of the database.
     * @param username The username to access the database with.
     * @param password The password associated with the username.
     */
    public Database(String url, String database, String username, String password)
    {
        try
        {
            Class.forName("org.postgresql.Driver");
        }
        catch (ClassNotFoundException e)
        {
            System.out.println("Postgres JDBC Driver not found.");
            e.printStackTrace();
            System.exit(-1);
        }

        try
        {
            connection = DriverManager.getConnection("jdbc:postgresql://" + url + "/" + database, username, password);
            if (connection != null)
                System.out.println("Connected to the database.");
            else
            {
                System.out.println("Failed to make connection!");
                System.exit(-1);
            }
        }
        catch (SQLException e)
        {
            System.out.println("Socket Failed! Check output console");
            System.exit(-1);
        }

        /*
         * Create the necessary database tables if they do not already exist.
         */
        String traceroute_query = "";
        traceroute_query += "CREATE TABLE IF NOT EXISTS traceroute (";
        traceroute_query += "id BIGSERIAL PRIMARY KEY,";
        traceroute_query += "source INET NOT NULL,";
        traceroute_query += "destination INET NOT NULL,";
        traceroute_query += "UNIQUE(source, destination));";
        this.command(traceroute_query);

        String hop_query = "";
        hop_query += "CREATE TABLE IF NOT EXISTS hop (";
        hop_query += "id BIGSERIAL PRIMARY KEY,";
        hop_query += "traceroute BIGSERIAL REFERENCES traceroute(id) NOT NULL,";
        hop_query += "hop SMALLINT NOT NULL,";
        hop_query += "responder INET NOT NULL,";
        hop_query += "UNIQUE(traceroute, hop, responder));";
        this.command(hop_query);

        // Note: Hop is deliberately not made a foreign key, since this is not possible with partitioned tables.
        String trace_query = "";
        trace_query += "CREATE TABLE IF NOT EXISTS trace (";
        trace_query += "hop BIGSERIAL NOT NULL,";
        trace_query += "delay SMALLINT NOT NULL,";
        trace_query += "timestamp TIMESTAMP WITH TIME ZONE NOT NULL)";
        trace_query += "PARTITION BY RANGE (timestamp);";
        this.command(trace_query);
    }

    /**
     * Insert a whole traceroute.
     */
    public void insert(JsonObject traceroute)
    {
        String source = traceroute.get("source").getAsString();
        String destination = traceroute.get("destination").getAsString();

        long ID = getTraceroute(source, destination);

        for(JsonElement x : traceroute.get("traces").getAsJsonArray())
        {
            JsonObject hop = (JsonObject)x;
            if(hop.get("rtt").getAsString().equals("*"))
                continue;

            // Insert or select the current hop.
            long HID = getHop(ID, hop.get("hop").getAsShort(), hop.get("responder").getAsString());

            // Insert a new trace.
            insertTrace(HID, (int)Math.ceil(hop.get("rtt").getAsDouble()), hop.get("timestamp").getAsLong());

        }
    }

    /**
     * Insert and/or retrieve a Traceroute entry.
     * @param source
     * @param destination
     * @return
     */
    private long getTraceroute(String source, String destination)
    {
        try
        {
            String query = "INSERT INTO traceroute(source, destination) VALUES('" + source + "', '" + destination + "') ON CONFLICT DO NOTHING RETURNING id";
            Statement statement = connection.createStatement();
            statement.execute(query);

            Statement S = connection.createStatement();
            String select = "SELECT id FROM traceroute WHERE source='" + source + "' AND destination='" + destination + "';";
            ResultSet keys = S.executeQuery(select);
            keys.next();
            return keys.getLong(1);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
            return -1;
        }
    }

    /**
     * Insert and/or receive a Hop entry.
     * @param traceroute
     * @param hop
     * @param responder
     * @return
     */
    private long getHop(long traceroute, short hop, String responder)
    {
        try
        {
            String query = "INSERT INTO hop(traceroute, hop, responder) VALUES(" + traceroute + ", " + hop + ", '" + responder + "') ON CONFLICT DO NOTHING";
            Statement statement = connection.createStatement();
            statement.execute(query);

            Statement S = connection.createStatement();
            String select = "SELECT id FROM hop WHERE traceroute=" + traceroute + " AND hop=" + hop + " AND responder='" + responder + "';";
            ResultSet keys = S.executeQuery(select);
            keys.next();
            return keys.getLong(1);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
            return -1;
        }
    }

    /**
     * Insert a traceroute entry and create a partition when necessary.
     * @param hop
     * @param delay
     * @param timestamp
     * @return
     */
    private void insertTrace(long hop, int delay, long timestamp)
    {
        /* -----------------------------------
         * Check if the partition already exists.
         * -----------------------------------*/
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        String mmyy = new SimpleDateFormat("MMyy").format(calendar.getTime());
        try
        {
            DatabaseMetaData meta = connection.getMetaData();
            ResultSet result = meta.getTables(null, null, "trace_" + mmyy , new String[] {"TABLE"});
            if(!result.isBeforeFirst())
            {
                String start = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()) + " 00:00:00";
                calendar.add(Calendar.MONTH, 1);
                String end = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()) + " 00:00:00";
                String query = "CREATE TABLE trace_" + mmyy + " PARTITION OF trace FOR VALUES FROM ('" + start + "') TO ('" + end + "');";
                command(query);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        try
        {
            if(delay > Short.MAX_VALUE)
                delay = Short.MAX_VALUE;


            String query = "INSERT INTO trace(hop, delay, timestamp) VALUES(" + hop + ", " + delay + ", '" + new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date(timestamp)) + "');";
            Statement statement = connection.createStatement();
            statement.execute(query);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
        }
    }


    /**
     * Sends a query to the database.
     * @param query
     * @return
     */
    public ResultSet query(String query)
    {
        try
        {
            Statement statement = connection.createStatement();
            return statement.executeQuery(query);
        }
        catch(SQLException exception)
        {
            exception.printStackTrace();
            return null;
        }
    }

    public void command(String query)
    {
        try
        {
            Statement statement = connection.createStatement();
            statement.execute(query);
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            System.exit(-1);
        }
    }
}