//package atlas;
//
//import com.google.gson.JsonArray;
//import com.google.gson.JsonElement;
//import com.google.gson.JsonObject;
//import com.google.gson.JsonParser;
//import com.google.gson.stream.JsonReader;
//import system.database.Database;
//
//import javax.net.ssl.HttpsURLConnection;
//import java.io.InputStreamReader;
//import java.net.URL;
//import java.util.ArrayList;
//
//public class Atlas
//{
//    // Database connection to the database.
//    private final Database database = null;
//
//    /**
//     * Currently fetches the measurements
//     */
//    public static void main(String[] argv) throws Exception
//    {
//        int pages = 1;
//        long results_since = 1512392440;
//        long results_till = 1512392500;
//
//        // Creates an atlas.Atlas object that can fetch and store traceroute information.
//        Atlas atlas = new Atlas();
//
//        // 1. Fetch all the Traceroute measurements from atlas.Atlas.
//        ArrayList<JsonObject> measurements = atlas.fetchTraceroutes(pages);
//
//        System.out.println("Amount of Measurements found: " + measurements.size());
//
//        // 2. Loop over each Traceroute measurement, retrieve results and store in database.
//        for(JsonObject measurement : measurements)
//        {
//            System.out.println("Inserting traceroute measurement #" + measurement.get("id").getAsLong() + " ...");
//        a    atlas.storeTraceroute(measurement.get("id").getAsLong(), results_since, results_till);
//        }
//    }
//
//    private Atlas()
//    {
//
//    }
//
//    /**
//     * Retrieves the measurement results and
//     */
//    private void storeTraceroute(long id, long results_since, long results_till) throws Exception
//    {
//
//        String url = "https://atlas.ripe.net/api/v2/measurements/" + id + "/results/?format=json&start=" + results_since + "&stop=" + results_till;
//
//        JsonReader reader = this.getJsonReader(url);
//        reader.beginArray();
//        JsonParser parser = new JsonParser();
//        while (reader.hasNext())
//        {
//            JsonElement message = parser.parse(reader);
//
//            String source = message.getAsJsonObject().get("src_addr").getAsString();
//            String destination = message.getAsJsonObject().get("dst_addr").getAsString();
//            long timestamp = message.getAsJsonObject().get("timestamp").getAsLong();
//            long ID = database.(message.getAsJsonObject());
//
//            JsonArray resultset = message.getAsJsonObject().get("result").getAsJsonArray();
//
//            for(JsonElement X : resultset)
//            {
//                int hop = X.getAsJsonObject().get("hop").getAsInt();
//
//                if(!X.getAsJsonObject().has("result"))
//                    continue;
//
//                JsonArray probes = X.getAsJsonObject().get("result").getAsJsonArray();
//                for(JsonElement probe : probes)
//                {
//                    int delay = 0;
//                    if(probe.getAsJsonObject().has("rtt"))
//                        delay = (int) (probe.getAsJsonObject().get("rtt").getAsDouble() * 1000000);
//
//                    if(probe.getAsJsonObject().has("from"))
//                    {
//                        String responder = probe.getAsJsonObject().get("from").getAsString();
//                        String trace = "INSERT INTO trace(traceroute, timestamp, hop, delay, responder) VALUES(" + ID + ", to_timestamp(" + timestamp + "), " + hop + ", " + delay + ", '" + responder + "');";
//                        database.command(trace);
//                    }
//
//                }
//            }
//        }
//
//        reader.endArray();
//        reader.close();
//
//    }
//
//    private ArrayList<JsonObject> fetchTraceroutes(int pages) throws Exception
//    {
//        ArrayList<JsonObject> measurements = new ArrayList<>();
//        for(int i = 1; i <= pages; i++)
//        {
//            System.out.println("Page: " + i);
//            String url = "https://atlas.ripe.net/api/v2/measurements/?format=json&TYPE=traceroute&page=" + i;
//
//            JsonParser parser = new JsonParser();
//            JsonElement element = parser.parse(getJsonReader(url));
//
//            for (JsonElement x : element.getAsJsonObject().get("results").getAsJsonArray())
//                measurements.add(x.getAsJsonObject());
//        }
//        return measurements;
//    }
//
//    private JsonReader getJsonReader(String url) throws Exception
//    {
//        HttpsURLConnection connection = (HttpsURLConnection) new URL(url).openConnection();
//        connection.setRequestMethod("GET");
//        connection.setDoInput(true);
//
//        return new JsonReader(new InputStreamReader(connection.getInputStream()));
//    }
//}
