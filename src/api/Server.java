package api;

import api.endpoints.Paths;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import core.Engine;

import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class Server
{
    private HttpServer server;

    public Server(int port) throws Exception
    {
        server = HttpServer.create(new InetSocketAddress(port), 0);

        /*
         * POSTing to this endpoint will create a new measurement on the RIPE Atlas server.
         */
        server.createContext("/measurements/", (HttpExchange e) ->
        {
            OutputStream output = e.getResponseBody();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            switch(e.getRequestMethod())
            {
                // Retrieve all currently ongoing Measurements
                case "GET":
                    String response_get = gson.toJson(Engine.getEngine().getMeasurements());
                    e.sendResponseHeaders(200, response_get.length());
                    output.write(response_get.getBytes());
                    break;

                case "POST":
                    JsonObject request = (JsonObject) new JsonParser().parse(new InputStreamReader(e.getRequestBody(), "UTF-8"));
                    String response_post = gson.toJson(Engine.getEngine().createMeasurement(request));
                    e.sendResponseHeaders(200, response_post.length());
                    output.write(response_post.getBytes());
                    break;

                default:
                    String error = "This operation is not supported.";
                    e.sendResponseHeaders(405, error.length());
                    output.write(error.getBytes());

            }
            output.close();
        });

        /*
         * Endpoint: /paths/pairs/
         */
        server.createContext("/paths/pairs/", Paths::pairs);

        server.setExecutor(null); // creates a default executor
        server.start();
    }
}
