package providers;

import collector.Connector;
import com.google.gson.JsonObject;
import core.Engine;
import system.database.Database;

import java.net.InetAddress;
import java.net.URL;
import java.util.concurrent.*;

/**
 * An interface to the TraceCollector. Can be used to query random /24 prefixes.
 */
public class TraceCollector
{
    /** The connector being used to connect to the TraceCollector. */
    private Connector connector = new Connector();

    /** The Threadpool which will be used to wait for a Future's response. */
    private ThreadPoolExecutor service;

    /** The source of the traceroutes. */
    private String source;

    /**
     * Constructs a
     * @param url The host to which we should connect.
     * @param user The SSH user to which we should connect.
     * @param local The local port on which we should use SSH port forwarding.
     * @param remote The remote port on which we should use SSH port forwarding.
     * @param key The location of the SSH key.
     * @param password The password to unlock the SSH key.
     * @param buffer The amount of requests to do simultaneously.
     * @throws Exception Any exceptions being thrown in the process.
     */
    public TraceCollector(String url, String user, int local, int remote, String key, String password, int buffer) throws Exception
    {
        InetAddress address = InetAddress.getByName(url);
        source = address.getHostAddress();

        connector.setSSHPortForward(user, url, local, remote, key, password);
        connector.connect("localhost", local, buffer);

        service = new ThreadPoolExecutor(buffer,
                buffer,
                Long.MAX_VALUE,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(buffer));
        service.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
    }

    /**
     * A blocking call that keeps requesting all /24 traceroutes at a TraceCollector.
     * These will be inserted in the Postgres database.
     * Note: This will be done in such a matter that the prefixes being queried are spread out as much as possible.
     * @throws Exception Any exceptions that occur during doing random traceroutes.
     */
    public void random() throws Exception
    {
        // Retrieve an instance of the database.
        Database database = Engine.getEngine().getDatabase();

        for(int x = 0; x < 256; x++)
            for(int y = 0; y < 256; y++)
                for(int z = 0; z < 256; z++)
                {
                    Future<JsonObject> future = connector.request(z + "." + y + "." + x + ".0");
                    service.submit(() ->
                    {
                        try
                        {
                            JsonObject result = future.get();
                            result.addProperty("source", source);
                            System.out.println(result.toString());
                            database.insert(result);
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    });
                }

        for(int i = 0; i < 256; i++)
        {
            System.out.println(i);
            Thread.sleep(25);
            connector.request("8.8.8." + i).toString();
        }
    }
}
