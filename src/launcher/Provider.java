package launcher;

import api.Server;
import collector.Connector;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import providers.TraceCollector;
import system.database.Database;

public class Provider
{
    // The connection to the Database
    private static Database database = null;

    // The server that clients use to connect to the
    private static Server server = null;

    /**
     * The main entry point of the program.
     * @param argv currently unused.
     * @throws Exception Any exceptions that occur during
     */
    public static void main(String[] argv) throws Exception
    {
        TraceCollector collector = new TraceCollector("192.42.130.21",
                "cveenman",
                8000,
                8000,
                "/srv/tracedb/ssh/key",
                "papier100",
                10);
        collector.random();
    }

    /**
     * Stores the Traceroute information into the database.
     * @param measurement
     */
    /*
    public static void store(JsonObject measurement)
    {
        String source = measurement.get("src_addr").getAsString();
        String destination = measurement.get("dst_addr").getAsString();
        long timestamp = measurement.get("timestamp").getAsLong();
        long ID = database.insert(source, destination);

        JsonArray resultset = measurement.get("result").getAsJsonArray();

        for(JsonElement X : resultset)
        {
            int hop = X.getAsJsonObject().get("hop").getAsInt();

            if(!X.getAsJsonObject().has("result"))
                continue;

            JsonArray probes = X.getAsJsonObject().get("result").getAsJsonArray();
            for(JsonElement probe : probes)
            {
                int delay = 0;
                if(probe.getAsJsonObject().has("rtt"))
                    delay = (int) (probe.getAsJsonObject().get("rtt").getAsDouble() * 1000000);

                if(probe.getAsJsonObject().has("from"))
                {
                    String responder = probe.getAsJsonObject().get("from").getAsString();
                    String trace = "INSERT INTO trace(traceroute, timestamp, hop, delay, responder) VALUES(" + ID + ", to_timestamp(" + timestamp + "), " + hop + ", " + (delay / 1000000) + ", '" + responder + "') ON CONFLICT DO NOTHING;";
                    database.command(trace);
                }
            }
        }
    }*/
}
