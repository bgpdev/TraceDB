package system.database;

import java.sql.ResultSet;

public interface IDatabase {

    void create();
    void retrieve();
    void update();
    void delete();
    ResultSet query(String query);
}
