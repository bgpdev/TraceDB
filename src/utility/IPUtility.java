package utility;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class IPUtility
{
    /**
     * Validates an IP Address
     * @param ip The IP address to validate, can be either IPv4 or IPv6.
     * @return true if the IP address is a valid IP address, false otherwise.
     */
    public static boolean validateIP(String ip)
    {
        try
        {
            InetAddress.getByName(ip);
        }
        catch (UnknownHostException e)
        {
            return false;
        }
        return true;
    }

    public static boolean isIPv6(String ip)
    {
        try
        {
            return InetAddress.getByName(ip).getAddress().length == 16 ? true : false;
        }
        catch (UnknownHostException e)
        {
            e.printStackTrace();
            System.exit(-1);
            return false;
        }
    }

    /**
     * Calculates the last IPv4 address the belongs to a IPv4 prefix.
     * @param ipv4 The starting IPv4 address.
     * @param prefix The prefix of this IPv4 prefix.
     * @return The last IPv4 address that belongs to the given IPv4 prefix.
     * @throws UnknownHostException An internal error that should never occur. (Should fix this)
     */
    public static String getEndingIPv4(String ipv4, int prefix) throws UnknownHostException
    {
        long x = IPv4toLong(ipv4);
        x += Math.pow(2, 32 - prefix) - 1;
        return InetAddress.getByName(String.valueOf(x)).getHostAddress();
    }

    /**
     * Calculates the last IPv6 address the belongs to a IPv6 prefix.
     * @param ipv6 The starting IPv6 address.
     * @param prefix The prefix of this IPv6 prefix.
     * @return The last IPv6 address that belongs to the given IPv6 prefix.
     * @throws UnknownHostException An internal error that should never occur. (Should fix this)
     */
    public static String getEndingIPv6(String ipv6, int prefix) throws Exception
    {
        BigInteger value = Ipv6ToBigInteger(ipv6);
        value = value.add(new BigInteger("2").pow(128 - prefix)).subtract(BigInteger.ONE);
        return InetAddress.getByAddress(value.toByteArray()).getHostAddress();
    }

    /**
     * Converts an IPv4 address to a long.
     * @param addr The IPv4 address that should be converted.
     * @return The IPv4 converted to a long.
     */
    private static long IPv4toLong(String addr)
    {
        String[] addrArray = addr.split("\\.");

        long num = 0;

        for (int i=0;i<addrArray.length;i++) {

            int power = 3-i;

            num += ((Integer.parseInt(addrArray[i])%256 * Math.pow(256,power)));

        }

        return num;
    }

    /**
     * Converts an IPv6 address to a BigInteger.
     * @param addr The IPv6 address that should be converted.
     * @return The IPv6 address converted to a BigInteger.
     */
    private static BigInteger Ipv6ToBigInteger(String addr) throws Exception
    {
        InetAddress a = InetAddress.getByName(addr);
        byte[] bytes = a.getAddress();
        return new BigInteger(1, bytes);
    }
}
