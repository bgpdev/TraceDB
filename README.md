# README.md

## Introduction
The WHOIS project is aimed at storing WHOIS records locally so they can be retrieved quicker without placing a burden on the WHOIS servers.
The project contains a WHOIS scraper that can scrape the contents of RADb and ARIN.
Since RADb already mirrors all IRR's (the ARIN whois database is not in sync with the ARIN IRR database that RADb uses so that is why we will parse it separately as well.)
A Dockerfile is present to run the application easily. See "Configuring the WHOIS scraper" for more details.

If any issues are found, please submit them at Gitlab.

## Initializing a local PostgreSQL database.
If you do not have an external PostgreSQL database that you want to use,
you can setup a local PostgreSQL database by using the docker-compose.yml already present in the repository.

To start PostgreSQL locally, run the following command:

```
docker-compose up -d postgres
```

PostgreSQL is configured to run on port 5432.
To change these ports or the configuration of your local setup, change the docker-compose.yml file locally.

**Note:** If you are not using a local PostgreSQL database but rather an external one,
you can skip these steps and set the POSTGRESQL_URL environment variable to the host of the external PostgreSQL database.


## Configuring TraceDB
TODO

```
PARAM: VALUE
```

To run TraceDB from the docker-compose.yml file, use the following commands.
TraceDB will use sensible defaults if the environment variables specified above were not already set.

```
docker-compose up -d tracedb
```

To run TraceDB with Docker only:
```
docker build -t tracedb_tracedb .
docker run -d -e <ENV_VAR_HERE>=<VALUE> whois_tracedb
```

To specify multiple environment variables use multiple -e arguments.

## Endpoints

### /paths/pairs/
Input
```
destination: <IP>/<prefix>
since: <epoch>                  [Optional] Default: Since the beginning
till: <epoch>                   [Optional] Default: Forever.
```

### /paths/graph/