#
# This Dockerfile is used to build the final production image of the TraceDB.
#

FROM gradle:alpine AS gradle

# Set the working directory to /srv/collector
WORKDIR /srv/tracedb/

# Set the current user of the image to 'root'
USER root

# Copy over the project to the Dockerfile
COPY . /srv/tracedb/

# Create a java executable by running installDist
RUN gradle installDist

#
# The final stage of the Multistage Docker image.
# By using multistage builds we wipe everything from before clean.
#

FROM openjdk:alpine

# Set the working directory again.
WORKDIR /srv/tracedb/

# Copy over the resources and the binary executable.
COPY --from=gradle /srv/tracedb/ssh/ /srv/tracedb/ssh/
COPY --from=gradle /srv/tracedb/build/install/tracedb/ /srv/tracedb/

# Run the TraceDB program.
CMD ["/srv/tracedb/bin/tracedb"]
