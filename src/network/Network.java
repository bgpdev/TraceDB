package network;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class Network
{
    private HashMap<String, Node> nodes = new HashMap<>();

    /** The final destination of the network. */
    private final Node destination;

    /**
     * Node class, represents a node in between.
     */
    public class Node
    {
        // The ip address of this node.
        public final String ip;

        // The upstream nodes (to the destination)
        public ArrayList<Relation> upstream = new ArrayList<>();

        // The downstream nodes (to the source)
        public ArrayList<Relation> downstream = new ArrayList<>();


        /**
         * Constructs a node with an empty set of relations.
         * @param ip The IP address of this node. TODO: Beware for traceroute timeouts!
         */
        public Node(String ip)
        {
            this.ip = ip;
        }

        private void addRelation(Node node, long timestamp)
        {
            upstream.add(new Relation(this, node, timestamp));
            node.downstream.add(new Relation( this, node, timestamp));
        }

        public void add(String responder, int hop, long timestamp)
        {
            Node latest = this;

            /*
             * Traverse the network as far as we can.
             */
            while(hop - 1 != 0)
            {
                if(latest.upstream.size() == 0)
                    break;

                Relation max = latest.upstream.get(0);
                for(Relation r : latest.upstream)
                    if(max.timestamp < r.timestamp)
                        max = r;

                latest = max.destination;

                hop--;
            }

            /*
             * Create new nodes for non-present nodes.
             */
            while(hop - 1 != 0)
            {
                Node intermediary = new Node("*");
                latest.addRelation(intermediary, timestamp);
                latest = intermediary;
                hop--;
            }

            /*
             * Insert the last message.
             */
            if(!nodes.containsKey(responder))
                nodes.put(responder, new Node(responder));

            // The root of a message.
            Node last = nodes.get(responder);

            latest.addRelation(last, timestamp);
        }

        public void printDownstream(int layer)
        {
            for(int i = 0; i < layer; i++)
                System.out.print('\t');

            System.out.println("- " + ip);
            for(Relation r : downstream)
                r.destination.printDownstream(layer + 1);
        }

        public void printUpstream(int layer)
        {
            for(int i = 0; i < layer; i++)
                System.out.print('\t');

            System.out.println("- " + ip);
            for(Relation r : upstream)
                r.destination.printUpstream(layer + 1);
        }
    }

    public class Relation
    {
        // The destination of this relation.
        public final Node source;

        // The destination of this relation.
        public final Node destination;

        // The timestamp of when this relationship was made.
        public final long timestamp;

        /**
         * Constructs a relation
         * @param destination
         */
        public Relation(Node source, Node destination, long timestamp)
        {
            this.source = source;
            this.destination = destination;
            this.timestamp = timestamp;
        }
    }

    /**
     * The constructor that constructs a network for a specific destination
     * @param destination
     */
    public Network(String destination)
    {
        this.destination = new Node(destination);
        nodes.put(destination, this.destination);
    }

    public void add(String source, String responder, int hop, long timestamp)
    {
        // #1: If the node is not present yet, add it to the network.
        if(!nodes.containsKey(source))
            nodes.put(source, new Node(source));

        // The root of a message.
        Node root = nodes.get(source);
        root.add(responder, hop, timestamp);
    }

    public String getDestination()
    {
        return destination.ip;
    }

    public ArrayList<Relation> getRelations()
    {
        ArrayList<Relation> relations = new ArrayList<>();
        Queue<Node> todo = new LinkedList<>();
        todo.add(destination);

        while(!todo.isEmpty())
        {
            Node node = todo.remove();
            System.out.println("Node IP: " + destination.ip);

            for(Relation x : node.downstream)
            {
                System.out.println("Nodes: " + x.source);
                relations.add(x);
                todo.add(x.source);
            }
        }

        return relations;
    }

    public void print()
    {
        destination.printDownstream(0);
    }
}
