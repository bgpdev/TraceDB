package api.endpoints;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.sun.net.httpserver.HttpExchange;
import core.Engine;
import network.Network;
import system.database.Database;
import utility.IPUtility;

import java.io.*;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Paths
{

    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }
    /**
     * The /paths/pairs endpoint.
     */
    public static void pairs(HttpExchange exchange)
    {
        try
        {
            OutputStream output = exchange.getResponseBody();

            /*
             * Parse and validate the Request.
             */



            JsonElement element = new JsonParser().parse(new JsonReader(new InputStreamReader(exchange.getRequestBody())));
            System.out.println(element.toString());
            if(element == null || !(element instanceof JsonObject))
            {
                String response = "{'error': 'No request object found.'}";
                exchange.sendResponseHeaders(400, response.length());
                output.write(response.getBytes());
                return;
            }

            JsonObject request = element.getAsJsonObject();

            if (!request.has("destination"))
            {
                String response = "{'error': 'property `destination` not present.'}";
                exchange.sendResponseHeaders(400, response.length());
                output.write(response.getBytes());
                return;
            }

            String prefix = request.get("destination").getAsString();

            if(!prefix.contains("/") || !IPUtility.validateIP(prefix.split("/")[0]))
            {
                String response = "{'error': 'property `destination` is not correctly formatted.'}";
                exchange.sendResponseHeaders(400, response.length());
                output.write(response.getBytes());
                return;
            }

            /*
             * Calculate some intermediary values.
             */
            int prefix_size = Integer.parseInt(prefix.split("/")[1]);
            String start = prefix.split("/")[0];
            String end = IPUtility.isIPv6(start) ? IPUtility.getEndingIPv6(start, prefix_size) : IPUtility.getEndingIPv4(start, prefix_size);


            /*
             * Retrieve the global Database connection, so that we can receive Traceroutes.
             */
            Database database = Engine.getEngine().getDatabase();
            System.out.println("Start: " + start + ", End: " + end);
            String query = "SELECT *, extract(epoch from timestamp) AS time FROM trace T JOIN traceroute R ON R.id = T.traceroute WHERE R.destination >= '" + start + "' AND R.destination <= '" + end + "' ORDER BY R.destination, T.hop, T.timestamp";
            System.out.println(query);
            ResultSet set = database.query(query);


            /*
             * Construct the response.
             */
            JsonObject response = new JsonObject();
            response.addProperty("prefix", prefix);
            response.add("destinations", new JsonArray());

            if(!set.isBeforeFirst())
            {
                String x = response.toString();
                exchange.sendResponseHeaders(200, x.length());
                output.write(x.getBytes());
                return;
            }

            Network network = null;
            JsonObject destination = null;
            while (set.next())
            {

                if(network == null)
                {
                    System.out.println("New Destination Set!: " + set.getString("destination"));
                    network = new Network(set.getString("destination"));
                    destination = new JsonObject();
                    destination.addProperty("destination", set.getString("destination"));
                    destination.add("pairs", new JsonArray());
                }

                if(!network.getDestination().equals(set.getString("destination")))
                {
                    System.out.println("New Destination Set!: " + set.getString("destination"));
                    ArrayList<Network.Relation> pairs = network.getRelations();
                    for(Network.Relation r : pairs)
                    {
                        JsonObject pair = new JsonObject();
                        pair.addProperty("source", r.source.ip);
                        pair.addProperty("destination", r.destination.ip);
                        pair.addProperty("timestamp", r.timestamp);
                        destination.get("pairs").getAsJsonArray().add(pair);
                    }

                    response.add("destinations", destination);
                    destination = new JsonObject();
                    destination.addProperty("destination", set.getString("destination"));
                    destination.add("pairs", new JsonArray());

                    network = new Network(set.getString("destination"));
                }
                network.add(set.getString("source"), set.getString("responder"), set.getShort("hop"), set.getLong("time"));
            }
            System.out.println("Out");
            {
                ArrayList<Network.Relation> pairs = network.getRelations();
                System.out.println("Out");
                for (Network.Relation r : pairs)
                {
                    JsonObject pair = new JsonObject();
                    pair.addProperty("source", r.source.ip);
                    pair.addProperty("destination", r.destination.ip);
                    pair.addProperty("timestamp", r.timestamp);
                    System.out.println("Pairs: " + pair);
                    destination.get("pairs").getAsJsonArray().add(pair);
                }

                response.add("destinations", destination);
            }

            String x = response.toString();
            exchange.sendResponseHeaders(200, x.length());
            output.write(x.getBytes());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
