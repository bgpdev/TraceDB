package core;

import atlas.API;
import atlas.Measurement;
import com.google.gson.JsonObject;
import system.database.Database;

import java.util.ArrayList;

public class Engine
{
    // The static instance of the engine.
    private static Engine engine = null;

    private Database database;

    /**
     *
     */
    private Engine()
    {
        database = new Database("postgres", "Traces", "TraceDB", "password");
    }

    /**
     * Retrieves the static Engine that keeps hold of all the Measurements being executed.
     * @return
     */
    public static Engine getEngine()
    {
        if(engine == null)
            engine = new Engine();
        return engine;
    }

    public Database getDatabase()
    {
        return database;
    }

    public JsonObject getMeasurements()
    {
        try
        {
            return API.getMeasurements();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public JsonObject createMeasurement(JsonObject request)
    {
        try
        {
            return API.createTraceroute(request);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
