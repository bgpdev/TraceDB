package atlas;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class Measurement
{
    private JsonObject measurement;

    // The number of nodes from which the traceroute should be executed.
    private static final int NODES = 1;



    /**
     * Constructor
     */
    private Measurement(JsonObject measurement)
    {
        this.measurement = measurement;
        System.out.println(measurement);
    }

    public static Measurement createMeasurement(long id) throws Exception
    {
        JsonObject result = API.getMeasurement(id);
        System.out.println(result);
        return new Measurement(result);
    }

    public JsonObject getResults() throws Exception
    {
        JsonObject result = API.getMeasurement(11157468);
        System.out.println(result.get("status").getAsJsonObject().get("name").getAsString());
        while(result.get("status").getAsJsonObject().get("name").getAsString().equals("Scheduled"))
        {
            Thread.sleep(10000);
            result = API.getMeasurement(11157021);
            System.out.println(result.get("status").getAsJsonObject().get("name").getAsString());
        }

        return null;
    }
}
