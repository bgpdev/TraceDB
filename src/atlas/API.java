package atlas;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class API
{
    // The key that is used to
    // TODO: Should NOT be committed
    private static String key = "d2ed6d94-2e00-4299-9bf9-b89517006318";

    public static JsonObject createTraceroute(JsonObject request) throws Exception
    {
        // Open a HTTP connection to the RIPE Atlas API.
        URL url = new URL("https://atlas.ripe.net/api/v2/measurements/?key=" + key);
        HttpURLConnection http = (HttpURLConnection) url.openConnection();
        http.setRequestMethod("POST"); // PUT is another valid option
        http.setRequestProperty("Content-Type", "application/json");
        http.setRequestProperty("Accept", "application/json");
        http.setDoOutput(true);

        /**
         * Create an input JSON Request
         */

        if(!request.has("definitions"))
        {
            JsonObject error = new JsonObject();
            error.addProperty("error", "'definitions' field is not present.");
            return error;
        }

        if(request.get("definitions").getAsJsonArray().size() == 0)
            request.get("definitions").getAsJsonArray().add(new JsonObject());

        for(JsonElement i : request.get("definitions").getAsJsonArray())
        {
            JsonObject x = (JsonObject) i;

            if (!x.has("target"))
            {
                JsonObject error = new JsonObject();
                error.addProperty("error", "Missing definition::target field.");
                return error;
            }

            if (!x.has("description"))
                x.addProperty("description", "Auto-generated one-time measurement.");

            if (!x.has("af"))
                x.addProperty("af", "4");

            // Always set the measurement type to 'traceroute'.
            x.addProperty("type", "traceroute");

            // Always set oneoff to false, to save credits.
            x.addProperty("one-off", false);

            // Never fragment traceroutes.
            x.addProperty("dont_fragment", false);

            if (!x.has("packets"))
                x.addProperty("packets", 1);

            // The interval between probes.
            // Note: It is currently larger then the stop - starttime, such that only one measurement is being done.
            if (!x.has("interval"))
                x.addProperty("interval", 3600 * 24);
        }

        /*
         * The probe section of the request object.
         */
        if(!request.has("probes"))
            request.add("probes", new JsonArray());

        if(request.get("probes").getAsJsonArray().size() == 0)
            request.get("probes").getAsJsonArray().add(new JsonObject());

        for(JsonElement i : request.get("probes").getAsJsonArray())
        {
            JsonObject x = (JsonObject)i;
            if (!x.has("requested"))
                x.addProperty("requested", 1);

            if (!x.has("type"))
                x.addProperty("type", "area");

            if (!x.has("value"))
                x.addProperty("value", "WW");
        }
        /*
         * Construct the final request object.
         */

        new DataOutputStream(http.getOutputStream()).writeBytes(request.toString());

        InputStream stream;
        if (http.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST)
            stream = http.getInputStream();
        else
            stream = http.getErrorStream();

        return (JsonObject)new JsonParser().parse(new JsonReader(new InputStreamReader(stream, "UTF-8")));
    }

    public static JsonObject getMeasurement(long ID) throws Exception
    {
        URL url = new URL("https://atlas.ripe.net/api/v2/measurements/" + ID + "/?format=json&key=" + key);
        URLConnection con = url.openConnection();
        HttpURLConnection http = (HttpURLConnection) con;

        http.setRequestMethod("GET"); // PUT is another valid option
        http.setRequestProperty("Content-Type", "application/json");
        http.setRequestProperty("Accept", "application/json");

        InputStream stream;
        if (http.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST)
            stream = http.getInputStream();
        else
            stream = http.getErrorStream();

        return (JsonObject)new JsonParser().parse(new InputStreamReader(stream, "UTF-8"));
    }

    public static JsonObject getMeasurements() throws Exception
    {
        URL url = new URL("https://atlas.ripe.net/api/v2/measurements/my/?format=json&key=" + key);
        URLConnection con = url.openConnection();
        HttpURLConnection http = (HttpURLConnection) con;

        http.setRequestMethod("GET"); // PUT is another valid option
        http.setRequestProperty("Content-Type", "application/json");
        http.setRequestProperty("Accept", "application/json");

        InputStream stream;
        if (http.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST)
            stream = http.getInputStream();
        else
            stream = http.getErrorStream();

        return (JsonObject)new JsonParser().parse(new InputStreamReader(stream, "UTF-8"));
    }


    public static JsonArray getResults(long ID) throws Exception
    {
        URL url = new URL("https://atlas.ripe.net/api/v2/measurements/" + ID + "/results/?format=json&key=" + key);
        URLConnection con = url.openConnection();
        HttpURLConnection http = (HttpURLConnection) con;

        http.setRequestMethod("GET"); // PUT is another valid option
        http.setRequestProperty("Content-Type", "application/json");
        http.setRequestProperty("Accept", "application/json");

        InputStream stream;
        if (http.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST)
            stream = http.getInputStream();
        else
            stream = http.getErrorStream();

        return (JsonArray)new JsonParser().parse(new InputStreamReader(stream, "UTF-8"));
    }
}
